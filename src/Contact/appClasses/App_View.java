package Contact.appClasses;

import java.util.Locale;
import java.util.TreeSet;
import java.util.logging.Logger;
import Contact.ServiceLocator;
import Contact.abstractClasses.View;
import Contact.commonClasses.Translator;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.collections.SetChangeListener.Change;

/**
 * Copyright 2015, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-clause license (see the file
 * license.txt).
 * 
 * @author Brad Richards
 */
public class App_View extends View<App_Model> {

	// menu
	Menu menuFile;
	Menu menuFileLanguage;
	Menu menuHelp;

	// control
	Button btnAddContact;
	Button btnDelete;

	// table
	public TableView<Contact> table;
	public TableColumn actionCol;
	public TableColumn<Contact, String> firstnameCol;
	public TableColumn<Contact, String> lastnameCol;
	public ObservableSet<Contact> items;

	public ObservableSet<Contact> getItems() {
		return items;
	}

	// update contact fields
	TextField tfUpdateFirstName = new TextField();
	TextField tfUpdateLastName = new TextField();
	TextField tfUpdatePhoneHome = new TextField();
	TextField tfUpdatePhoneHandy = new TextField();
	TextField tfUpdatePhoneBusiness = new TextField();
	TextField tfUpdateemail1 = new TextField();
	TextField tfUpdateemail2 = new TextField();
	Label lblUpdateLastName = new Label();
	Label lblUpdateFirstName = new Label();
	Label lblUpdatePhoneHome = new Label();
	Label lblUpdatePhoneHandy = new Label();
	Label lblUpdatePhoneBusiness= new Label();
	Label lblUpdateemail1 = new Label();
	Label lblUpdateemail2 = new Label();
	Stage updateStage = new Stage();
	public IntegerProperty IdObjectToUpdate;
	Button btnUpdate = new Button();
	public TextField tfUpdatePhone = new TextField();



	// addContact fields
	Stage addContactStage = new Stage();
	Label lblFirstName = new Label();
	Label lblLastName = new Label();
	Label lblPhoneHome = new Label();
	Label lblPhoneHandy = new Label();
	Label lblPhoneBusiness = new Label();
	Label lblemail1 = new Label();
	Label lblemail2 = new Label();
	Label lblMail = new Label();
	TextField tfFirstName = new TextField();
	TextField tfLastName = new TextField();
	TextField tfPhoneHome = new TextField();
	TextField tfPhoneHandy = new TextField();
	TextField tfPhoneBusiness = new TextField();
	TextField tfemail1 = new TextField();
	TextField tfemail2 = new TextField();
	public Button btnAddContactToList = new Button();
	

	public App_View(Stage stage, App_Model model) {
		super(stage, model);
		ServiceLocator.getServiceLocator().getLogger().info("Application view initialized");
	}

	@Override
	protected Scene create_GUI() {
		ServiceLocator sl = ServiceLocator.getServiceLocator();
		Logger logger = sl.getLogger();

		MenuBar menuBar = new MenuBar();
		menuFile = new Menu();
		menuFileLanguage = new Menu();
		menuFile.getItems().add(menuFileLanguage);

		for (Locale locale : sl.getLocales()) {
			MenuItem language = new MenuItem(locale.getLanguage());
			menuFileLanguage.getItems().add(language);
			language.setOnAction(event -> {
				sl.getConfiguration().setLocalOption("Language", locale.getLanguage());
				sl.setTranslator(new Translator(locale.getLanguage()));
				updateTexts();
			});
		}

		menuHelp = new Menu();
		menuBar.getMenus().addAll(menuFile, menuHelp);

		BorderPane root = new BorderPane();
		GridPane grid = new GridPane();
		root.setTop(menuBar);

		btnAddContact = new Button();
		btnAddContact.setMinWidth(200);
		btnDelete = new Button();
		btnDelete.setMinWidth(50);
		grid.add(btnAddContact, 0, 0);
		grid.add(btnDelete, 1, 0);
		root.setBottom(grid);
		root.setCenter(creatContactListPane());
		updateTexts();
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("app.css").toExternalForm());
		return scene;
	}

	private TableView<Contact> createTableView(ObservableSet<Contact> items) {

		this.table = new TableView<>();
		table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		model.itemList = FXCollections
				.observableArrayList(contact -> new Observable[] { contact.getFirstname() });

		items.addListener((Change<? extends Contact> c) -> {
			if (c.wasAdded()) {
				model.itemList.add(c.getElementAdded());
			}
			if (c.wasRemoved()) {
				model.itemList.remove(c.getElementRemoved());
			}
			
		});

		// usual column setup
		firstnameCol = new TableColumn<>();
		firstnameCol.setCellValueFactory(cellData -> cellData.getValue().getFirstname());
		lastnameCol = new TableColumn<>();
		lastnameCol.setCellValueFactory(cellData -> cellData.getValue().getLastname());

		// use an unmodifiable list for the table to prevent any direct updates to the
		// table's list (updates must go through the set)
		table.setItems(FXCollections.unmodifiableObservableList(model.itemList));

		// add any existing elements:
		model.itemList.addAll(items);

		return table;
	}

	// creats the main pane with the table, where the contacts get added
	public Pane creatContactListPane() {

		Translator t = ServiceLocator.getServiceLocator().getTranslator();

		this.items = FXCollections.observableSet(new TreeSet<Contact>());
		TableView<Contact> table = createTableView(items);

		VBox pane = new VBox();
		table.setEditable(true);
		actionCol = new TableColumn<>();
		actionCol.setCellValueFactory(new PropertyValueFactory<>(""));

		// this method is adapted from: //
		// https://docs.oracle.com/javafx/2/ui_controls/table-view.htm
		Callback<TableColumn<Contact, String>, TableCell<Contact, String>> cellFactory = new Callback<TableColumn<Contact, String>, TableCell<Contact, String>>() {

			@Override
			public TableCell<Contact, String> call(final TableColumn<Contact, String> param) {
				final TableCell<Contact, String> cell = new TableCell<Contact, String>() {

					public Button btnEdit = new Button();
					

					@Override
					public void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						btnEdit.setText(t.getString("btn.edit"));
						btnEdit.setId("my-editbutton");
						if (empty) {
							setGraphic(null);
							setText(null);
						} else {
							btnEdit.setOnAction(event -> {					
								Contact contact = getTableView().getItems().get(getIndex());
								updateDataDisplayPane(contact);
							});
							setGraphic(btnEdit);
							setText(null);
						}
					}
				};
				updateTexts();
				return cell;
			}
		};

		actionCol.setCellFactory(cellFactory);

		table.getColumns().addAll(firstnameCol, lastnameCol, actionCol);
		table.setItems(model.itemList);

		pane.getChildren().add(table);
		updateTexts();
		return pane;
	}



	// the pane to add a new contact
	public Pane addContactPane() {
		addContactStage.setWidth(600);
		addContactStage.setHeight(400);

		GridPane pane = new GridPane();
		pane.add(lblFirstName, 0, 1);
		pane.add(tfFirstName, 1, 1);
		pane.add(lblLastName, 0, 2);
		pane.add(tfLastName, 1, 2);
		pane.add(lblPhoneHome, 0, 3);
		pane.add(tfPhoneHome, 1, 3);
		pane.add(lblPhoneHandy, 0, 4);
		pane.add(tfPhoneHandy, 1, 4);
		pane.add(lblPhoneBusiness, 0, 5);
		pane.add(tfPhoneBusiness, 1, 5);
		pane.add(lblemail1, 0, 6);
		pane.add(tfemail1, 1, 6);
		pane.add(lblemail2, 0, 7);
		pane.add(tfemail2, 1, 7);
		pane.add(btnAddContactToList, 0, 8);


		Scene scene = new Scene(pane);
		addContactStage.setScene(scene);
		addContactStage.show();

		ServiceLocator.getServiceLocator().getLogger().info("AddContactPane initialized");
		updateTexts();
		return pane;

	}
	
	// pane to make a change on a contact;
	public Pane updateDataDisplayPane(Contact contact) {
		updateStage.setWidth(300);
		updateStage.setHeight(400);
		GridPane pane = new GridPane();

		this.tfUpdateFirstName.setText(contact.getFirstname().getValue());		
		this.tfUpdateLastName.setText(contact.getLastname().getValue());
		this.tfUpdatePhoneHome.setText(contact.getPhoneHome().getValue());
		this.tfUpdatePhoneHandy.setText(contact.getPhoneHandy().getValue());
		this.tfUpdatePhoneBusiness.setText(contact.getPhoneBusiness().getValue());
		this.tfUpdateemail1.setText(contact.getEmail1().getValue());
		this.tfUpdateemail2.setText(contact.getEmail2().getValue());
		this.IdObjectToUpdate = contact.getID();
		
		pane.add(lblUpdateFirstName, 0, 0);
		pane.add(tfUpdateFirstName, 1, 0);
		pane.add(lblUpdateLastName, 0, 1);
		pane.add(tfUpdateLastName, 1, 1);
		pane.add(lblUpdatePhoneHome, 0, 2);
		pane.add(tfUpdatePhoneHome, 1, 2);
		pane.add(lblUpdatePhoneHandy, 0, 3);
		pane.add(tfUpdatePhoneHandy, 1, 3);
		pane.add(lblUpdatePhoneBusiness, 0, 4);
		pane.add(tfUpdatePhoneBusiness, 1, 4);
		pane.add(lblUpdateemail1, 0, 5);
		pane.add(tfUpdateemail1, 1, 5);
		pane.add(lblUpdateemail2, 0, 6);
		pane.add(tfUpdateemail2, 1, 6);
		pane.add(btnUpdate, 0, 7);

		
		Scene scene = new Scene(pane);
		updateStage.setScene(scene);
		updateStage.show();

		ServiceLocator.getServiceLocator().getLogger().info("Update-Pane initialized");
		updateTexts();
		return pane;

	}

	protected void updateTexts() {
		Translator t = ServiceLocator.getServiceLocator().getTranslator();

		// The menu entries
		menuFile.setText(t.getString("program.menu.file"));
		menuFileLanguage.setText(t.getString("program.menu.file.language"));
		menuHelp.setText(t.getString("program.menu.help"));

		// Other controls
		btnAddContact.setText(t.getString("button.btnAddContact"));
		btnDelete.setText(t.getString("button.delete"));

		// addContactPane
		if(firstnameCol != null && lastnameCol!=null && lblPhoneHome!=null && lblPhoneHandy!=null && lblPhoneBusiness!=null && lblemail1!=null) {
		this.lblFirstName.setText(t.getString("addContact.firstname"));
		this.lblLastName.setText(t.getString("addContact.lastname"));
		this.lblPhoneHome.setText(t.getString("addContact.phoneHome"));
		this.lblPhoneHandy.setText(t.getString("addContact.phoneHandy"));
		this.lblPhoneBusiness.setText(t.getString("addContact.phoneBusiness"));
		this.lblemail1.setText(t.getString("addContact.email1"));
		this.lblemail2.setText(t.getString("addContact.email2"));
		this.btnAddContactToList.setText(t.getString("addContact.addContactToList"));
		}
		
		// updatePane
		if(lblUpdateFirstName!=null && lblUpdateLastName!=null  && lblUpdatePhoneHome!=null && lblUpdatePhoneHandy!=null && lblUpdatePhoneBusiness!=null && lblUpdateemail1!=null && lblUpdateemail2!=null) {
		this.lblUpdateFirstName.setText(t.getString("update.firstname"));
		this.lblUpdateLastName.setText(t.getString("update.lastname"));
		this.lblUpdatePhoneHome.setText(t.getString("update.phoneHome"));
		this.lblUpdatePhoneHandy.setText(t.getString("update.phoneHandy"));
		this.lblUpdatePhoneBusiness.setText(t.getString("update.phoneBusiness"));
		this.lblUpdateemail1.setText(t.getString("update.email1"));
		this.lblUpdateemail2.setText(t.getString("update.email2"));
		this.btnUpdate.setText(t.getString("update.update"));
		}
		
		if (firstnameCol != null && lastnameCol != null && actionCol!=null) {
			actionCol.setText(t.getString("table.actionCol"));
			firstnameCol.setText(t.getString("table.firstnameCol"));
			lastnameCol.setText(t.getString("table.lastnameCol"));
		}

		stage.setTitle(t.getString("program.name"));
	}
}