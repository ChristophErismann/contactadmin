package Contact.appClasses;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.imageio.event.IIOReadUpdateListener;

import Contact.ServiceLocator;
import Contact.abstractClasses.Controller;
import Contact.commonClasses.Translator;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.WindowEvent;

/**
 * Copyright 2015, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-clause license (see the file
 * license.txt).
 * 
 * @author Brad Richards
 */
public class App_Controller extends Controller<App_Model, App_View> {
	ServiceLocator serviceLocator = ServiceLocator.getServiceLocator();
	ObservableList<Contact> selection = view.table.getSelectionModel().getSelectedItems();

	public App_Controller(App_Model model, App_View view) {
		super(model, view);
		// register ourselves to handle window-closing event
		view.getStage().setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				Platform.exit();
			}
		});
		view.btnAddContact.setOnAction(this::addContact);
		view.btnAddContactToList.setOnAction(this::addContactToList);
		view.btnDelete.setOnAction(this::delete);
		view.btnUpdate.setOnAction(this::update);
		serviceLocator.getLogger().info("Application controller initialized");
	}

	private void addContact(ActionEvent e) {
		view.addContactPane();
	}

	private void addContactToList(ActionEvent e) {

		try {
			String firstname = view.tfFirstName.getText();
			String lastname = view.tfLastName.getText();
			String phoneHome = view.tfPhoneHome.getText();
			String phoneHandy = view.tfPhoneHandy.getText();
			String phoneBusiness = view.tfPhoneBusiness.getText();
			String email1 = view.tfemail1.getText();
			String email2 = view.tfemail2.getText();

			if (validateMail1(email1) == false) {
				Translator t = ServiceLocator.getServiceLocator().getTranslator();
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText(t.getString("alert.mail.false"));
				alert.showAndWait();
				view.tfemail1.setText(email1);

			} else {
				serviceLocator.getLogger().info("Mail Address initiallised");
			}

			if (validateMail2(email2) == false) {
				Translator t = ServiceLocator.getServiceLocator().getTranslator();
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText(t.getString("alert.mail.false"));
				alert.showAndWait();
				view.tfemail2.setText(email2);

			} else {
				serviceLocator.getLogger().info("Mail Address initiallised");
				view.addContactStage.close();
			}

			if (validatePhoneHome(phoneHome) == false) {
				Translator t = ServiceLocator.getServiceLocator().getTranslator();
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText(t.getString("alert.phone.false"));
				alert.showAndWait();
				view.tfPhoneHome.setText("");

			} else {
				serviceLocator.getLogger().info("Phone Address initiallised");
			}

			if (validatePhoneHandy(phoneHandy) == false) {
				Translator t = ServiceLocator.getServiceLocator().getTranslator();
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText(t.getString("alert.phone.false"));
				alert.showAndWait();
				view.tfPhoneHandy.setText("");

			} else {
				serviceLocator.getLogger().info("Phone Address initiallised");
			}

			if (validatePhoneBusiness(phoneBusiness) == false) {
				Translator t = ServiceLocator.getServiceLocator().getTranslator();
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText(t.getString("alert.phone.false"));
				alert.showAndWait();
				view.tfPhoneBusiness.setText("");

			} else {
				serviceLocator.getLogger().info("Phone Address initiallised");
			}

			if (!firstname.isEmpty() && !lastname.isEmpty()) {
				saveContact(firstname, lastname, phoneHome, phoneHandy, phoneBusiness, email1, email2);
			} else {
				Translator t = ServiceLocator.getServiceLocator().getTranslator();
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText(t.getString("alert.emptyfields"));
				alert.showAndWait();
			}
			view.tfFirstName.clear();
			view.tfLastName.clear();
			view.tfPhoneHome.clear();
			view.tfPhoneHandy.clear();
			view.tfPhoneBusiness.clear();
			view.tfemail1.clear();
			view.tfemail2.clear();

		} catch (NoSuchElementException e1) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText(e1.toString());
			alert.showAndWait();
			view.tfFirstName.setText("");
			view.tfLastName.setText("");

		}

	}

	public void saveContact(String firstname, String lastname, String phoneHome, String phoneHandy,
			String phoneBusiness, String email1, String email2) {

		Contact c = new Contact(firstname, lastname, phoneHome, phoneHandy, phoneBusiness, email1, email2);
		// validator for duplicates
		if (!equals(c)) {
			view.items.add(c);
			serviceLocator.getLogger().info("Saving contact successfully: " + c);
			view.updateStage.close();

		}
	}

	private boolean equals(Contact c) {
		// Create an Iterator over the TreeSet
		Iterator<Contact> iterator = view.items.iterator();

		// Loop over the TreeSet values and print the values
		while (iterator.hasNext()) {
			if (iterator.next().getFirstname().getValue().equals(c.getFirstname().getValue())
					&& iterator.next().getLastname().getValue().equals(c.getLastname().getValue())) {
				return true;
			}
		}
		return false;
	}

	private void update(ActionEvent e) {
		String firstname = view.tfUpdateFirstName.getText();
		String lastname = view.tfUpdateLastName.getText();
		String phoneHome = view.tfUpdatePhoneHome.getText();
		String phoneHandy = view.tfUpdatePhoneHandy.getText();
		String phoneBusiness = view.tfUpdatePhoneBusiness.getText();
		String email1 = view.tfUpdateemail1.getText();
		String email2 = view.tfUpdateemail2.getText();

		int id = view.IdObjectToUpdate.get();

		if (deleteFromTreeSet(id)) {
			if (!firstname.isEmpty() && !lastname.isEmpty()) {
				saveContact(firstname, lastname, phoneHome, phoneHandy, phoneBusiness, email1, email2);

			} else {
				Translator t = ServiceLocator.getServiceLocator().getTranslator();
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText(t.getString("alert.emptyfields"));
				alert.showAndWait();

				view.updateStage.close();
			}
		}

	}

	private boolean deleteFromTreeSet(int id) {
	
		for (Contact c : view.items) {
			if (c.getID().get() == id) {
				view.items.remove(c);
				return true;
			}
		}
		return false;
	}

	private void delete(ActionEvent e) {
		view.items.removeIf((new TreeSet<Contact>(selection)::contains));
		serviceLocator.getLogger().info("Object removed: " + selection.toString());
	}

	private boolean validateMail1(String email1) {
		if (!email1.isEmpty()) {
			return email1.matches("^\\S+@\\S+$");
		} else {
			return true;
		}
	}

	private boolean validateMail2(String email2) {
		if (!email2.isEmpty()) {
			return email2.matches("^\\S+@\\S+$");
		} else {
			return true;
		}
	}

	private boolean validatePhoneHome(String phoneHome) {
		if (!phoneHome.isEmpty()) {
			return phoneHome.length() == 10 && phoneHome.matches("[0-9]+");
		} else {
			return true;
		}
	}

	private boolean validatePhoneHandy(String phoneHandy) {
		if (!phoneHandy.isEmpty()) {
			return phoneHandy.length() == 10 && phoneHandy.matches("[0-9]+");
		} else {
			return true;
		}
	}

	private boolean validatePhoneBusiness(String phoneBusiness) {
		if (!phoneBusiness.isEmpty()) {
			return phoneBusiness.length() == 10 && phoneBusiness.matches("[0-9]+");
		} else {
			return true;
		}
	}

}
