package Contact.appClasses;

import java.util.Optional;
import Contact.ServiceLocator;
import Contact.abstractClasses.Model;
import javafx.collections.ObservableList;


/**
 * Copyright 2015, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-clause license (see the file
 * license.txt).
 * 
 * @author Brad Richards
 */
public class App_Model extends Model {
	ServiceLocator serviceLocator;
	private int value;
	
	public ObservableList<Contact> itemList;
	


	public ObservableList<Contact> getItemList() {
		return itemList;
	}

	public void setItemList(ObservableList<Contact> itemList) {
		this.itemList = itemList;
	}

	public App_Model() {
		value = 0;
		serviceLocator = ServiceLocator.getServiceLocator();
		serviceLocator.getLogger().info("Application model initialized");
	}

	public int getValue() {
		return value;
	}

	public int incrementValue() {
		value++;
		serviceLocator.getLogger().info("Application model: value incremented to " + value);
		return value;
	}
/*
	public void updateContact(String firstname, String lastname, int id) {
		Optional<Contact> contactToUpdate = view.item.stream()
				.filter(canton -> contact.getID().get() == id).findFirst();
				
		contactToUpdate.get().setFirstname(firstname);
		contactToUpdate.get().setLastname(lastname);
		
		
	}
}*/
}
