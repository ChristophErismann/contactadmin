package Contact.appClasses;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Contact implements Comparable<Contact> {

	private StringProperty lastname;
	private StringProperty firstname;
	private StringProperty phoneHome;
	private StringProperty phoneHandy;
	private StringProperty phoneBusiness;
	private StringProperty email1;
	private StringProperty email2;



	
	private final IntegerProperty ID;
	private StringProperty email;

	public Contact(String firstname, String lastname, String phoneHome, String phoneHandy, String phoneBusiness, String email1, String email2) {
		this.firstname = new SimpleStringProperty(firstname);
		this.lastname = new SimpleStringProperty(lastname);
		this.phoneHome = new SimpleStringProperty(phoneHome);
		this.phoneHandy = new SimpleStringProperty(phoneHandy);
		this.phoneBusiness = new SimpleStringProperty(phoneBusiness);
		this.email1 = new SimpleStringProperty(email1);
		this.email2 = new SimpleStringProperty(email2);

		this.ID = new SimpleIntegerProperty(getNextID());

	}

	private static int highestID = -1;

	public static int getHighestID() {
		return highestID;
	}

	// Class method to get next available ID
	private static int getNextID() {
		highestID = highestID + 1;
		return highestID;
	}

	/// ---- Getters and Setters ----////
	public StringProperty getLastname() {
		return lastname;
	}

	public void setLastname(StringProperty lastname) {
		this.lastname = lastname;
	}

	public StringProperty getFirstname() {
		return firstname;
	}

	public void setFirstname(StringProperty firstname) {
		this.firstname = firstname;
	}

	public StringProperty getEmail() {
		return email;
	}

	public void setEmail(StringProperty email) {
		this.email = email;
	}

	public IntegerProperty getID() {
		return ID;
	}
	
	public StringProperty getPhoneHome() {
		return phoneHome;
	}

	public void setPhoneHome(StringProperty phoneHome) {
		this.phoneHome = phoneHome;
	}

	public StringProperty getPhoneHandy() {
		return phoneHandy;
	}

	public void setPhoneHandy(StringProperty phoneHandy) {
		this.phoneHandy = phoneHandy;
	}

	public StringProperty getPhoneBusiness() {
		return phoneBusiness;
	}

	public void setPhoneBusiness(StringProperty phoneBusiness) {
		this.phoneBusiness = phoneBusiness;
	}

	public StringProperty getEmail1() {
		return email1;
	}

	public void setEmail1(StringProperty email1) {
		this.email1 = email1;
	}

	public StringProperty getEmail2() {
		return email2;
	}

	public void setEmail2(StringProperty email2) {
		this.email2 = email2;
	}

	public String toString() {
		return this.firstname + " " + this.lastname + " " + this.phoneHome + " "+ this.phoneHandy + " "+ this.phoneBusiness + " "+ this.email1 + " "+ this.email2 + " " +this.ID;

	}

	@Override
	public int compareTo(Contact o) {
		return this.getID().get() - o.getID().get();
	}

	@Override 
	public boolean equals(Object c2) {
		Contact c = (Contact) c2;
		return this.getFirstname().get() == c.getFirstname().get();
	}

}
